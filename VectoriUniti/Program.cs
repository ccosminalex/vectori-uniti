﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectoriUniti
{
    class Program
    {
        static void Main(string[] args)
        {
            int m, n;
            Console.Write("Introduceti m: ");
            m = int.Parse(Console.ReadLine());
            Console.Write("Introduceti n: ");
            n = int.Parse(Console.ReadLine());
            int[] v1 = new int[m];
            int[] v2 = new int[n];
            int[] v3 = new int[m + n];
            Random rand = new Random();
            for (int i = 0; i < v1.Length; i++)
            {
                v1[i] = rand.Next(100);
            }
            for (int i = 0; i < v2.Length; i++)
            {
                v2[i] = rand.Next(100);
            }
            bubbleSort(ref v1);
            bubbleSort(ref v2);
            /*foreach (int nr in v1)
                Console.Write(nr + " ");
            Console.WriteLine();
            foreach (int nr in v2)
                Console.Write(nr + " ");
                */
            int c1 = 0;
            int c2 = 0;
            int c3 = 0;
            while (c1 < v1.Length && c2 < v2.Length)
            {
                if (v1[c1] <= v2[c2])
                {
                    v3[c3++] = v1[c1++];
                }
                else
                {
                    v3[c3++] = v2[c2++];
                }

            }
            if (c1 < v1.Length)
            {
                for (int i = c1; i < v1.Length; i++)
                {
                    v3[c3++] = v1[i];
                }
            }
            else
                for (int i = c2; i < v2.Length; i++)
                {
                    v3[c3++] = v2[i];
                }
            Console.WriteLine();
            foreach (int nr in v3)
                Console.Write(nr + " ");
            Console.ReadKey();

        }
        static void bubbleSort(ref int[] v)
        {
            bool ok;
            do
            {
                ok = true;
                for (int i = 0; i < v.Length - 1; i++)
                {
                    if (v[i] > v[i + 1])
                    {
                        int c = v[i];
                        v[i] = v[i + 1];
                        v[i + 1] = c;
                        ok = false;
                    }
                }
            } while (!ok);
        }
    }
}
